fun radixSort(arr: Array<Int>): Array<Int> {
    val m = getMax(arr)
    var exp = 1

    while(m/exp > 0)
    {
        countSort(arr, exp)
        exp *= 10
    }

    return arr
}

private fun getMax(arr: Array<Int>): Int
{
    var max = 0

    for (i in 0 until arr.size)
        if (arr[i] > max)
            max = arr[i]

    return max
}

private fun countSort(arr: Array<Int>, exp: Int)
{
    val outArr: Array<Int> = Array(arr.size){0}
    val count: Array<Int> = Array(10){0}

    for(i in 0 until arr.size)
        count[(arr[i] / exp) % 10]++

    for(i in 1 until 10)
        count[i] += count[i - 1]

    for(i in arr.size - 1 downTo 0)
    {
        outArr[count[(arr[i] / exp) % 10] - 1] = arr[i]
        count[(arr[i] / exp) % 10]--
    }

    for(i in 0 until arr.size)
        arr[i] = outArr[i]
}