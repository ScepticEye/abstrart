package Discontinued

import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.imgcodecs.Imgcodecs

class SnP
{
    var inWidth: Int = 0
    var inHeight: Int = 0

    fun SaltNPepper(image: Array<IntArray>, perce: Int)
    {
        for (i in 0 until inWidth)
            for (j in 0 until inHeight)
            {
                val chance : Int = (0..99).random()

                if (chance in 0..perce)
                {
                    val worb : Int = (0..1).random()

                    if (worb == 0)
                        image[i][j] = 0
                    else if (worb == 1)
                        image[i][j] = 255
                }
            }
    }

    fun getImage(): Array<IntArray>
    {
        /** Get image from source */
        val inMat: Mat = Imgcodecs.imread(
            "C:\\Users\\joaof\\Documents\\IntelliJ_Projects\\Abstrart\\src\\images\\fruitSaladMed.png",
            CvType.CV_32SC1
        )

        /** Defines image size */
        inWidth = inMat.width()
        inHeight = inMat.height()

        val inArray: Array<IntArray> = Array(inWidth) { IntArray(inHeight) }

        /** Converts matrix to Array of IntArrays */
        for (i in 0 until inWidth) //"0 until inWidth" = "0..inWidth - 1"
            for (j in 0 until inHeight)
                inArray[i][j] = inMat[j, i][0].toInt()

        return inArray
    }

    fun outImage(image: Array<IntArray>)
    {
        /** Matrix is created based on the Array of IntArrays and the image is generated
         * i and j are switched on the Matrix because that's how they work */
        val outMat = Mat(inHeight, inWidth, CvType.CV_32SC1)

        for (i in 0 until inWidth)
            for (j in 0 until inHeight) //"0..inHeight - 1" can be converted to "0 until inHeight"
                outMat.put(j, i, image[i][j].toDouble())

        Imgcodecs.imwrite(
            "C:\\Users\\joaof\\Documents\\IntelliJ_Projects\\Abstrart\\src\\outputImgs\\outMed.png",
            outMat
        )
    }
}