package Discontinued

import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.imgcodecs.Imgcodecs.imread
import org.opencv.imgcodecs.Imgcodecs.imwrite

class Template
{
    var inWidth: Int = 0
    var inHeight: Int = 0

    fun FilterTemplate(image: Array<IntArray>, perce : Int)
    {

    }

    fun GetImage(imgSrc: String): Array<IntArray>
    {
        /** Get image from source */
        val inMat: Mat = imread(imgSrc, CvType.CV_32SC1)

        /** Defines image size */
        inWidth = inMat.width()
        inHeight = inMat.height()

        val inArray: Array<IntArray> = Array(inWidth) { IntArray(inHeight) }

        /** Converts matrix to Array of IntArrays */
        for (i in 0 until inWidth) //"0 until inWidth" = "0..inWidth - 1"
            for (j in 0 until inHeight)
                inArray[i][j] = inMat[j, i][0].toInt()

        return inArray
    }

    fun OutImage(image: Array<IntArray>)
    {
        /** Matrix is created based on the Array of IntArrays and the image is generated
         * i and j are switched on the Matrix because that's how they work */
        val outMat = Mat(inHeight, inWidth, CvType.CV_32SC1)

        for (i in 0 until inWidth)
            for (j in 0 until inHeight) //"0..inHeight - 1" can be converted to "0 until inHeight"
                outMat.put(j, i, image[i][j].toDouble())

        imwrite("outputImgs/outLar.png", outMat)
    }
}

private fun main()
{
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME)

    val imgSrc = "" //Path to image

    val filter = Template() //Instantiates the class

    val image: Array<IntArray> = filter.GetImage(imgSrc) //Get the image in Array of IntArray form

    val perce : Int = 0

    filter.FilterTemplate(image, perce) //Processes image

    filter.OutImage(image) //Creates image file
}
