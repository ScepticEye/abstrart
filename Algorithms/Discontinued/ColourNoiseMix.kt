package Discontinued

import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.imgcodecs.Imgcodecs.imread
import org.opencv.imgcodecs.Imgcodecs.imwrite

class ColourNoiseMix
{
    var inWidth: Int = 0
    var inHeight: Int = 0

    fun colourFilter(image: Array<Array<IntArray>>, perce: Int)
    {
        for (i in 0 until inWidth)
            for (j in 0 until inHeight)
            {
                val chance: Int = (0..99).random()

                if (chance in 0..perce)
                {
                    setColour(image, i, j)
                }
            }
    }

    private fun setColour(image: Array<Array<IntArray>>, i: Int, j: Int)
    {
        val cselect: Int = (0..2).random()
        val ranChannel: Int = (0..2).random()

        for (k in 0 until 3)
            if (k != cselect)
                image[i][j][k] = 0

        if (cselect != ranChannel)
            image[i][j][ranChannel] = image[i][j][cselect]
    }

    fun getImage(): Array<Array<IntArray>>
    {
        /** Get image from source */
        val inMat: Mat = imread("images/prp.jpg", CvType.CV_32SC3)

        /** Defines image size */
        inWidth = inMat.width()
        inHeight = inMat.height()

        val inArray: Array<Array<IntArray>> = Array(inWidth) { Array(inHeight){IntArray(3)} }

        /** Converts matrix to Array of IntArrays */
        for (i in 0 until inWidth) //"0 until inWidth" = "0..inWidth - 1"
            for (j in 0 until inHeight)
                for (k in 0 until 3)
                    inArray[i][j][k] = inMat[j, i][k].toInt()

        return inArray
    }

    fun outImage(image: Array<Array<IntArray>>)
    {
        /** Matrix is created based on the Array of IntArrays and the image is generated
         * i and j are switched on the Matrix because that's how they work */
        val outMat = Mat(inHeight, inWidth, CvType.CV_32SC3)

        for (i in 0 until inWidth)
            for (j in 0 until inHeight) //"0..inHeight - 1" can be converted to "0 until inHeight"
                outMat.put(j, i, image[i][j])

        imwrite("outputImgs/outPrp.png", outMat)
    }
}

private fun main()
{
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME)

    val filter = ColourNoiseMix()

    val image: Array<Array<IntArray>> = filter.getImage()

    val perce = 60

    filter.colourFilter(image, perce)

    filter.outImage(image)
}