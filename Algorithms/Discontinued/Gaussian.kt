package Discontinued

import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc

class Gaussian
{
    var inWidth: Int = 0
    var inHeight: Int = 0

    fun gaussian(image: Mat, perce : Int): Mat
    {
        /** Makes a 32 bit 3 chanel matrix with the original image size */
        val outMat = Mat(inHeight, inWidth, CvType.CV_32SC3)

        /** Applies the GaussianBlur from the OpenCV library */
        Imgproc.GaussianBlur(
            image,
            outMat,
            Size(perce.toDouble(), perce.toDouble()),
            1.0
        ) //the higher the size, the more intense the blur becomes being 99 the max value

        return outMat
    }

    fun getImage(): Mat
    {
        /** Get image from source */
        val inMat: Mat = Imgcodecs.imread(
            "C:\\Users\\joaof\\Documents\\IntelliJ_Projects\\Abstrart\\src\\images\\fruitSaladMed.png",
            CvType.CV_32SC3
        )

        /** Defines image size */
        inWidth = inMat.width()
        inHeight = inMat.height()

        return inMat
    }
}

private fun main()
{
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME)

    val filter = Discontinued.Gaussian()

    val image = filter.getImage()

    //Overall percentage of noise
    val perce = 39 //to the user shows up as 40 but because we use here from 0 to 99 and the numbers can only be positive and odd

    Imgcodecs.imwrite(
        "C:\\Users\\joaof\\Documents\\IntelliJ_Projects\\Abstrart\\src\\outputImgs\\outMed.png",
        filter.gaussian(image, perce)
    )
}