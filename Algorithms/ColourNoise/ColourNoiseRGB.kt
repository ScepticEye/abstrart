fun colourFilterRGB(image: Array<Array<Array<Int>>>, perce: Int, inWidth: Int, inHeight: Int): Array<Array<Array<Int>>> {
    for (i in 0 until inWidth)
        for (j in 0 until inHeight)
        {
            val chance: Int = (0..99).random()

            if (chance in 0..perce)
            {
                setColour(image, i, j)
            }
        }

    return image
}

private fun setColour(image: Array<Array<Array<Int>>>, i: Int, j: Int)
{
    val cselect: Int = (0..2).random()
    val ranChannel: Int = (0..2).random()

    for (k in 0 until 3)
        if (k != cselect)
            image[i][j][k] = 0

    if (cselect != ranChannel)
        image[i][j][ranChannel] = image[i][j][cselect]
}