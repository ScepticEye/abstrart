fun SaltNPepper(image: Array<Array<Int>>, perce: Int, inWidth: Int, inHeight: Int): Array<Array<Int>>
{
    for (i in 0 until inWidth)
        for (j in 0 until inHeight)
        {
            val chance : Int = (0..99).random()

            if (chance in 0..perce)
            {
                val worb : Int = (0..1).random()

                if (worb == 0)
                    image[i][j] = 0
                else if (worb == 1)
                    image[i][j] = 255
            }
        }
    return image
}
