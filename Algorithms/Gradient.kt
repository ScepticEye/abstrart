import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.imgcodecs.Imgcodecs.imread
import org.opencv.imgcodecs.Imgcodecs.imwrite

class Gradient
{
    var inWidth: Int = 0
    var inHeight: Int = 0

    fun gradient(image: Array<Array<Int>>)
    {
        println("Before flatten(): ${image.count()}")
        println("After flatten(): ${image.flatten().count()}")

        val flatImg = image.flatten()

        val sortedArr = radixSort(flatImg.toTypedArray())

        var flatPoint = 0

        for (i in 0 until  inWidth)
            for (j in 0 until  inHeight)
            {
                image[i][j] = sortedArr[flatPoint]
                flatPoint++
            }
    }

    fun getImage(imgSrc: String): Array<Array<Int>>
    {
        /** Get image from source */
        val inMat: Mat = imread(imgSrc, CvType.CV_32SC1)

        /** Defines image size */
        inWidth = inMat.width()
        inHeight = inMat.height()

        val inArray: Array<Array<Int>> = Array(inWidth) { Array(inHeight){0} }

        /** Converts matrix to Array of IntArrays */
        for (i in 0 until inWidth) //"0 until inWidth" = "0..inWidth - 1"
            for (j in 0 until inHeight)
                inArray[i][j] = inMat[j, i][0].toInt()

        return inArray
    }

    fun outImage(image: Array<Array<Int>>)
    {
        /** Matrix is created based on the Array of IntArrays and the image is generated
         * i and j are switched on the Matrix because that's how they work */
        val outMat = Mat(inHeight, inWidth, CvType.CV_32SC1)

        for (i in 0 until inWidth)
            for (j in 0 until inHeight) //"0..inHeight - 1" can be converted to "0 until inHeight"
                outMat.put(j, i, image[i][j].toDouble())

        imwrite("OutputImages/outLar.png", outMat)
    }
}

private fun main()
{
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME)

    val imgSrc = "Images/fruit_salad.jpg" //Path to image

    val filter = Gradient() //Instantiates the class

    val image: Array<Array<Int>> = filter.getImage(imgSrc) //Get the image in Array of IntArray form

    filter.gradient(image) //Processes image

    filter.outImage(image) //Creates image file
}