import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.imgcodecs.Imgcodecs

var inWidth: Int = 0
var inHeight: Int = 0

fun main()
{
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME)

    val srcPath = "C:\\Users\\joaof\\Documents\\IntelliJ_Projects\\Abstrart\\src\\images\\fruitSaladBig.png"
    val outputImage = "C:\\Users\\joaof\\Documents\\IntelliJ_Projects\\Abstrart\\src\\outputImgs\\outMed.png"

    /* SALT AND PEPPER APPLIED
    val img = getImageGrey(srcPath)

    val filtered_img = SaltNPepper(img, perce=15, inWidth, inHeight)

    outImageGrey(filtered_img, outputImage)
     */

    /* GAUSSIAN FILTER APPLIED
    val img = gaussianGetImage(srcPath)

    val filtered_img = Gaussian(img, perce = 11)

    gaussianOutImage(outputImage, filtered_img)
    */

    /* COLOUR FILTER RGB APPLIED
    val img = getImageRGB(srcPath)

    val filtered_img = colourFilterRGB(img, 20, inWidth, inHeight)

    outImageRGB(image = filtered_img, outputImage)
     */

    val img = getImageRGB(srcPath)

    val filtered_img = colourFilterMix(img, 20, inWidth, inHeight)

    outImageRGB(image = filtered_img, outputImage)
}

fun getImageGrey(srcPath: String): Array<Array<Int>>
{
    /** Get image from source */
    val inMat: Mat = Imgcodecs.imread(srcPath, CvType.CV_32SC1)

    /** Defines image size */
    inWidth = inMat.width()
    inHeight = inMat.height()

    val inArray: Array<Array<Int>> = Array(inWidth) { Array(inHeight){0} }

    /** Converts matrix to Array of IntArrays */
    for (i in 0 until inWidth) //"0 until inWidth" = "0..inWidth - 1"
        for (j in 0 until inHeight)
            inArray[i][j] = inMat[j, i][0].toInt()

    return inArray
}

fun outImageGrey(image: Array<Array<Int>>, outputPath: String)
{
    /** Matrix is created based on the Array of IntArrays and the image is generated
     * i and j are switched on the Matrix because that's how they work */
    val outMat = Mat(inHeight, inWidth, CvType.CV_32SC1)

    for (i in 0 until inWidth)
        for (j in 0 until inHeight) //"0..inHeight - 1" can be converted to "0 until inHeight"
            outMat.put(j, i, image[i][j].toDouble())

    Imgcodecs.imwrite(outputPath, outMat)
}

fun getImageRGB(srcPath: String): Array<Array<Array<Int>>>
{
    /** Get image from source */
    val inMat: Mat = Imgcodecs.imread(
        srcPath,
        CvType.CV_32SC3
    )

    /** Defines image size */
    inWidth = inMat.width()
    inHeight = inMat.height()

    val inArray: Array<Array<Array<Int>>> = Array(inWidth) { Array(inHeight){Array(3){Int.MAX_VALUE} } }

    /** Converts matrix to Array of IntArrays */
    for (i in 0 until inWidth) //"0 until inWidth" = "0..inWidth - 1"
        for (j in 0 until inHeight)
            for (k in 0 until 3)
                inArray[i][j][k] = inMat[j, i][k].toInt()

    return inArray
}

fun outImageRGB(image: Array<Array<Array<Int>>>, outputPath: String)
{
    /** Matrix is created based on the Array of IntArrays and the image is generated
     * i and j are switched on the Matrix because that's how they work */
    val outMat = Mat(inHeight, inWidth, CvType.CV_32SC3)

    for (i in 0 until inWidth)
        for (j in 0 until inHeight) //"0..inHeight - 1" can be converted to "0 until inHeight"
            outMat.put(j, i, image[i][j].toIntArray())

    Imgcodecs.imwrite(outputPath, outMat)
}