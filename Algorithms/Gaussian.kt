import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc

fun Gaussian(image: Mat, perce : Int): Mat
{
    /** Makes a 32 bit 3 chanel matrix with the original image size */
    val outMat = Mat(inHeight, inWidth, CvType.CV_32SC3)

    /** Applies the GaussianBlur from the OpenCV library */
    Imgproc.GaussianBlur(image, outMat, Size(perce.toDouble(), perce.toDouble()), 1.0) //the higher the size, the more intense the blur becomes being 99 the max value

    return outMat
}

fun gaussianGetImage(srcImage: String): Mat
{
    /** Get image from source */
    val inMat: Mat = Imgcodecs.imread(srcImage, CvType.CV_32SC3)

    /** Defines image size */
    inWidth = inMat.width()
    inHeight = inMat.height()

    return inMat
}

fun gaussianOutImage(outImage: String, filteredImage: Mat)
{
    Imgcodecs.imwrite(
        outImage,
        filteredImage
    )
}